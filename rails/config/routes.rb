Rails.application.routes.draw do
  get 'repas/new'

  get 'sessions/new'

  get 'pages/home'
  get 'pages/about'
  root 'pages#home'
  get '/signup', to: 'restos#new'
  post '/signup', to: 'restos#create'
  get '/login',   to: 'sessions#new'
  post '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :restos
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
