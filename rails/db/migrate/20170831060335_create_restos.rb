class CreateRestos < ActiveRecord::Migration[5.1]
  def change
    create_table :restos do |t|
      t.string :fname
      t.string :name
      t.string :resto
      t.string :email
      t.string :phone
      t.string :adresse
      t.string :website

      t.timestamps
    end
  end
end
