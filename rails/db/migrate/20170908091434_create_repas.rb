class CreateRepas < ActiveRecord::Migration[5.1]
  def change
    create_table :repas do |t|
      t.string :nmenu
      t.text :menu
      t.float :prix

      t.timestamps
    end
  end
end
