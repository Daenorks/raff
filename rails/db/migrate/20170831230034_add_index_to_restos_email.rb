class AddIndexToRestosEmail < ActiveRecord::Migration[5.1]
  def change
    add_index :restos, :email, unique: true
  end
end
