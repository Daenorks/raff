class SessionsController < ApplicationController
  def new
  end

  def create
    resto = Resto.find_by(email: params[:session][:email].downcase)
    if resto && resto.authenticate(params[:session][:password])
      log_in resto
      redirect_to resto
      # Log the resto in and redirect to the resto's show page.
    else
      flash[:danger] = 'Invalid email/password combination' # Not quite right!
      # Create an error message.
     render 'new'
    end
  end

  def destroy
    log_out
    flash[:sucess] = "logout succesfuly"
    redirect_to root_url
  end
end
