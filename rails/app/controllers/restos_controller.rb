class RestosController < ApplicationController
  respond_to :json, :html, only: [:show]

  def show
    @resto = Resto.find(params[:id])
    respond_with(@resto)
  end

  def new
    @resto = Resto.new
  end

  def create
    @resto = Resto.new(resto_params)
    if @resto.save
      log_in @resto
      flash[:sucess] = "nice"
      redirect_to @resto
    else 
      render 'new'
    end
  end

  private

    def resto_params
      params.require(:resto).permit(:name, :fname, :resto,
        :email, :phone, :adresse, :password, :password_confirmation)
    end
end
