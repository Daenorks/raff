module SessionsHelper
  def log_in(resto)
    session[:resto_id] = resto.id
  end

  def current_resto
    @current_resto ||= Resto.find_by(id: session[:resto_id])
  end

  def logged_in?
    !current_resto.nil?
  end

  def log_out
    session.delete(:resto_id)
    @current_user = nil
  end
end
